//
//  EvilHangmanModel.h
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EvilHangmanModel : NSObject {
@private
    bool _guessed[26];
    NSMutableDictionary *_equivalenceClasses;
}

@property (readwrite, nonatomic, assign) NSUInteger length;
@property (readonly, nonatomic, assign) NSUInteger guessesRemaining;

- (id)initWithContentsOfFile:(NSString *)file;
- (NSUInteger)longestWord;
- (NSString *)setWordLength:(NSUInteger)length andGuessesRemaining:(NSUInteger)guesses;
- (NSString *)respondToGuess:(char)letter;
- (void)resetIsGuessed;
- (Boolean)isGuessed:(char)letter;
- (void)setIsGuessed:(char)letter;
- (Boolean)didWin;
- (NSString *)possibleWord;
- (NSString *)getPlaceholders:(NSString *)word;
- (NSString *)getGuessedLetters;
- (void)printEquivalenceClasses;

@end
