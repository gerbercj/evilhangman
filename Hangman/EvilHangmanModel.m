//
//  EvilHangmanModel.m
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "EvilHangmanModel.h"
#import "NSString+EvilHangman.h"


@implementation EvilHangmanModel

@synthesize length=_length;
@synthesize guessesRemaining=_guessesRemaining;

- (id)initWithContentsOfFile:(NSString *)file
{    
    self = [super init];
    
    if (self) {
        // create a dictionary for equivalence classes (key of @"" is for out of play words)
        _equivalenceClasses = [[NSMutableDictionary alloc] init];
                
        NSLog(@"Path to property list: %@",file);
        
        // load all the words from the property list        
        NSMutableArray *wordList = [[NSMutableArray alloc] initWithContentsOfFile:file];
        NSLog(@"Number of words loaded: %u",[wordList count]);
        
        // store the words in the out-of-play equivalence class
        [_equivalenceClasses setObject:wordList
                                forKey:@""];
        
        // dictionary has a reference, so we can release ours
        
        // clear the list of guesses
        [self resetIsGuessed];
    }
    
    return self;
}

- (NSUInteger)longestWord
{
    // initialize result
    NSUInteger maxLength=0;
    
    // for each key in dictionary
    for (NSString *equivalenceClass in [_equivalenceClasses allKeys]) {
        // and for each word in the array for that key
        NSArray *wordList = [_equivalenceClasses objectForKey:equivalenceClass];
        for (NSString *word in wordList) {
            // if this word is longer
            if ([word length] > maxLength) {
                // update our max length
                maxLength=[word length];
            }
        }
    }
    
    return maxLength;
}

- (NSString *)setWordLength:(NSUInteger)length andGuessesRemaining:(NSUInteger)guesses
{
    NSLog(@"Setting word length to %u and guesses remaining to %u",length,guesses);
    
    // store word length and remaining guesses
    self.length=length;
    _guessesRemaining=guesses;

    // create a series of dashes for the default key
    NSString *emptyEquivalenceKey = [@"" stringByPaddingToLength:length 
                                                      withString: @"-"
                                                 startingAtIndex:0];
    
    // create new array to save words of proper length
    NSMutableArray *wordArray = [[NSMutableArray alloc] init];
    
    // create new array to save out of play words
    NSMutableArray *outOfPlayArray = [[NSMutableArray alloc] init];
    
    // for all the words all the word lists
    for (NSString *equivalenceClass in [_equivalenceClasses allKeys]) {
        NSMutableArray *wordList = [_equivalenceClasses objectForKey:equivalenceClass];
        for (NSUInteger index = [wordList count]; index!=0; index--) {
            
            // get the next word
            NSString *word = [wordList objectAtIndex:index-1];
            
            // if this word is the appropriate length
            if( [word length] == length ) {
                // insert the word into the dictionary with the appropriate equivalence class key
                [wordArray addObject:word];
            } else {
                // wrong length; leave out of play
                [outOfPlayArray addObject:word];
            }
            
            // remove word from word store
            [wordList removeObjectAtIndex:index-1];
        }
    }
    
    // put our in-play and out-of-play arrays in the dictionary
    [_equivalenceClasses setObject:wordArray forKey:emptyEquivalenceKey];
    [_equivalenceClasses setObject:outOfPlayArray forKey:@""];

    // dictionary has references, so release ours
    
    // clear the list of guesses
    [self resetIsGuessed];
    
    // return the placeholder for use by the GUI
    return emptyEquivalenceKey;
}

- (NSString *)respondToGuess:(char)letter
{
    NSMutableArray *wordList;
    NSString *placeholders =@"";
    NSString *equivalenceClass;
    NSString *word;
    NSUInteger index;
    
    NSLog(@"Guess was: %c",letter);
    // check if this is a duplicate guess
    if (_guessed[letter-'A'] == NO) {
        
        // record the guessed letter
        _guessed[letter-'A'] = YES;
        NSLog(@"Not a dupe; recording");
        
        // calculate new equivalence classes based upon guess
        for (equivalenceClass in [_equivalenceClasses allKeys]) {
            // skip out of play words
            if (![equivalenceClass isEqualToString:@""]) {
                
                wordList = [_equivalenceClasses objectForKey:equivalenceClass];
                for (index = [wordList count]; index!=0; index--) {
                    // get the next word
                    word = [wordList objectAtIndex:index-1];
                    // calculate the equivalence class for this word
                    placeholders = [self getPlaceholders:word];
                    if ([_equivalenceClasses objectForKey:placeholders] == nil) {
                        // no key for this placeholder yet; create and add word
//                        NSLog(@"Creating array for equivalence class %@",placeholders);
                        NSMutableArray *wordArray = [[NSMutableArray alloc]
                                                     initWithObjects:word, nil];
                        // store it in the dictionary
                        [_equivalenceClasses setObject:wordArray
                                                  forKey:placeholders];
                        
                        // remove from previous equivalence class
                        [wordList removeObjectAtIndex:index-1];
                        
                        // release our local reference counter
                    } else {
                        // only move words if they are in the wrong list
                        if (![equivalenceClass isEqualToString:placeholders]) {
                            // add word to existing key for placeholder
                            [[_equivalenceClasses objectForKey:placeholders] addObject:word];
                            // remove from previous equivalence class
                            [wordList removeObjectAtIndex:index-1];
                        }
                    }
                    // word is in an array, so remove our reference
                }
            }
        }
        
        // find largest/best equivalence class
        NSUInteger maxCount = 0;
        NSString *equivalenceClassForMaxCount=@"";
        // for all equivalence classes
        for ( equivalenceClass in [_equivalenceClasses allKeys] ) {
            // skip out of play words
            if (![equivalenceClass isEqualToString:@""]) {
                // get count of words for this equivalence class
                NSUInteger equivalenceClassCount = [[_equivalenceClasses objectForKey:equivalenceClass] count];
//                NSLog(@"equivalenceClass: %@; count: %u",equivalenceClass, equivalenceClassCount);
                // if the same length, use the one with more placeholders remaining
                if ( equivalenceClassCount == maxCount ) {
//                    NSLog(@"As good as current best");
                    if ( [equivalenceClass placeHolderCount] > [equivalenceClassForMaxCount placeHolderCount] ) {
//                        NSLog(@"New best");
                        equivalenceClassForMaxCount = equivalenceClass;
                    }
                }
                
                // if more alternatives, use that equivalence class
                if ( equivalenceClassCount > maxCount ) {
                    maxCount = equivalenceClassCount;
                    equivalenceClassForMaxCount = equivalenceClass;
                }
            }
        }
        
        // select the best equivalence class
        placeholders = equivalenceClassForMaxCount;
        NSLog(@"Selected equivalenceClass: %@",placeholders);
        
        // check if the player wound up getting a letter; a strike if not
        NSRange rangeOfGuessInPlaceholder = [placeholders rangeOfString:[NSString stringWithFormat:@"%c",letter]];
        if ( rangeOfGuessInPlaceholder.length == 0 ) {
            // letter was not a match
            _guessesRemaining--;
            NSLog(@"Letter was a strike (remaining=%u)", self.guessesRemaining);
        }
        
        // find the out-of-play array in the dictionary
        NSMutableArray *outOfPlayArray = [_equivalenceClasses objectForKey:@""];
        
        // move unneeded equivalence classes to out of play
        for ( equivalenceClass in [_equivalenceClasses allKeys] ) {
            // if this word is not out-of-play, and not in selected equivalence class
            if (![equivalenceClass isEqualToString:@""] && ![equivalenceClass isEqualToString:placeholders] ) {
                wordList = [_equivalenceClasses objectForKey:equivalenceClass];
                for (index = [wordList count]; index!=0; index--) {
                    
                    // get the next word
                    NSString *word = [wordList objectAtIndex:index-1];
                    
                    // insert the word into the out-of-play array
                    [outOfPlayArray addObject:word];
                                    }
                // remove all the above words in bulk (costs space, but saves time)
                [_equivalenceClasses removeObjectForKey:equivalenceClass];
//                NSLog(@"Moved %@ to out of play",equivalenceClass);
            }
        }
    } else {
        // duplicate guess; ignore, but return the old character class
        for (equivalenceClass in [_equivalenceClasses allKeys]) {
            if (![equivalenceClass isEqualToString:@""]) {
                placeholders = equivalenceClass;
            }
        }
    }
    return placeholders;
}

- (void)resetIsGuessed
{
    // for all possible letters
    for (NSUInteger index=0; index<26; index++) {
        // mark as not guessed
        _guessed[index] = NO;
    }
}

- (Boolean)isGuessed:(char)letter
{
    // check if the specified letter was guessed
    return _guessed[letter-'A'];
}

- (void)setIsGuessed:(char)letter
{
    // mark the specified letter as guessed
    _guessed[letter-'A'] = YES;
}

- (Boolean)didWin
{
    // check if there are any words without placeholders
    for (NSString* equivalenceClass in [_equivalenceClasses allKeys]) {
        // skip out of play words
        if (![equivalenceClass isEqualToString:@""]) {
            NSArray* wordArray = [_equivalenceClasses objectForKey:equivalenceClass];
            if ([wordArray count] > 1) {
                // can't be a winner if there's more than one choice left
                break;
            }
            
            NSRange rangeOfPlaceholderInWord = [equivalenceClass rangeOfString:@"-"];
            if (rangeOfPlaceholderInWord.length == 0) {
                // no placeholders in placeholder; WINNING!
                NSLog(@"didWin: YES");
                return YES;
            }
            // a placeholder was found; can't be a winner
            break;
            
        }
    }
    
    // did not win... yet
    NSLog(@"didWin: NO");
    return NO;
}

- (NSString *)possibleWord
{
    NSString* word = @"";
    
    // search all equivalence classes
    for (NSString* equivalenceClass in [_equivalenceClasses allKeys]) {
        // skip out of play words
        if (![equivalenceClass isEqualToString:@""]) {
            // the first word in a valid equivalence class is good enough
            word = [[_equivalenceClasses objectForKey:equivalenceClass] objectAtIndex:0];
            // don't search further
            break;
        }
    }
    
    return word;
}

- (NSString *)getPlaceholders:(NSString *)word
{
    // convert parameter to cstring
    const char *cStringWord = [word UTF8String];
    
    // cstring to store result
    char placeholder[_length+1];
    
    // for each character position
    for (NSUInteger charIndex=0; ; charIndex++) {
        // if the end of the cstring
        if (cStringWord[charIndex] == 0 || charIndex>=_length) {
            // terminate the result cstring
            placeholder[charIndex]=0;
            break;
        }
        // if the letter at the current position is a guessed letter
        if (_guessed[cStringWord[charIndex]-'A']) {
            // show the letter in the result string
            placeholder[charIndex] = cStringWord[charIndex];
        } else {
            // otherwise, show a placeholder in the result string
            placeholder[charIndex] = '-';
        }
    }
    
    // null terminate the generated cstring to be safe
    placeholder[_length]=0;
    
    // return the NSString translation of our cstring
    return [NSString stringWithUTF8String:placeholder];
}

- (NSString *)getGuessedLetters
{
    // create a cstring for the letters of the alphabet
    char guessedLetters[27];
    
    // for each letter in the alphabet
    for (NSUInteger index=0; index<26; index++) {
        // if the letter was guessed
        if (_guessed[index]) {
            // insert actual letter in the cstring
            guessedLetters[index]='A'+index;
        } else {
            // otherwise, insert a blank space in the cstring
            guessedLetters[index]=' ';
        }
    }
    
    // null terminate the generated cstring
    guessedLetters[26]=0;
    
    // return the NSString representation of the cstring
    return [NSString stringWithUTF8String:guessedLetters];
}

- (void)printEquivalenceClasses
{
    NSUInteger index;
    
    // for all the equivalence classes
    for( NSString* equivalenceClass in _equivalenceClasses ) {
        // skip out of play words
        if (![equivalenceClass isEqualToString:@""]) {
            // log the equivalence class name
            NSLog(@"Equivalence Class: %@", equivalenceClass);
            // get all the words in the equivalence class array
            NSArray *wordList = [_equivalenceClasses objectForKey:equivalenceClass]; 
            for( index = [wordList count]; index!=0; index--) {
                // log each word
                NSLog(@"  %@",[wordList objectAtIndex:index-1]);
            }
        }
    }
}

- (void)dealloc
{
    // deallocate our dictionary
    _equivalenceClasses=nil;
}

@end
