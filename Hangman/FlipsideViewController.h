//
//  FlipsideViewController.h
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FlipsideViewControllerDelegate;

@interface FlipsideViewController : UIViewController {
}

@property (nonatomic, unsafe_unretained) id <FlipsideViewControllerDelegate> delegate;
@property (nonatomic) IBOutlet UISlider *wordLength;
@property (nonatomic) IBOutlet UILabel *selectedWordLength;
@property (nonatomic) IBOutlet UISlider *maxGuesses;
@property (nonatomic) IBOutlet UILabel *selectedMaxGuesses;
@property (nonatomic) IBOutlet UILabel *longestWordLength;

- (void)updateWordLength:(NSUInteger)length;
- (void)updateMaxGuesses:(NSUInteger)guesses;

- (IBAction)done:(id)sender;
- (IBAction)selectWordLength:(id)sender;
- (IBAction)finalizeWordLength:(id)sender;
- (IBAction)selectMaxGuesses:(id)sender;
- (IBAction)finalizeMaxGuesses:(id)sender;

@end


@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
- (NSUInteger)getLongestWordLength;
@end
