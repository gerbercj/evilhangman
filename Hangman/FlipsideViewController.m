//
//  FlipsideViewController.m
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "FlipsideViewController.h"


@implementation FlipsideViewController

@synthesize delegate=_delegate;
@synthesize wordLength;
@synthesize selectedWordLength;
@synthesize maxGuesses;
@synthesize selectedMaxGuesses;
@synthesize longestWordLength;

# pragma mark - Instance methods


- (void)updateWordLength:(NSUInteger)length
{
    // set the position of the slider
    [wordLength setValue:length];
    
    // update the label
    [selectedWordLength setText:[NSString stringWithFormat:@"%i",length]];
}

- (void)updateMaxGuesses:(NSUInteger)guesses
{
    // set the position of the slider
    [maxGuesses setValue:guesses];
    
    // update the label
    [selectedMaxGuesses setText:[NSString stringWithFormat:@"%i",guesses]];
}

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // load user preferences
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // update UI with stored preferences
    [self updateWordLength:[defaults floatForKey:@"wordLength"]];
    [self updateMaxGuesses:[defaults floatForKey:@"maxGuesses"]];
    
    // ask main view controller for longest word length and set property on slider appropriately
    self.wordLength.maximumValue = [self.delegate getLongestWordLength];
}

- (void)viewDidUnload
{
    // don't need to store state of UI controls
    [self setWordLength:nil];
    [self setSelectedWordLength:nil];
    [self setMaxGuesses:nil];
    [self setSelectedMaxGuesses:nil];
    [self setLongestWordLength:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Only support portrait orientation
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    // tell the main view that we're done
    [self.delegate flipsideViewControllerDidFinish:self];
}

- (IBAction)selectWordLength:(id)sender
{
    // update UI with integer word length
    [self updateWordLength:roundf([wordLength value])];    
}

- (IBAction)finalizeWordLength:(id)sender
{
    // update stored preferences with new value
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:[wordLength value] forKey:@"wordLength"];
    [defaults synchronize];
}

- (IBAction)selectMaxGuesses:(id)sender
{
    // update UI with integer max guesses
    [self updateMaxGuesses:roundf([maxGuesses value])];
}

- (IBAction)finalizeMaxGuesses:(id)sender
{
    // update stored preferences with new value
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setFloat:[maxGuesses value] forKey:@"maxGuesses"];
    [defaults synchronize]; 
}

@end
