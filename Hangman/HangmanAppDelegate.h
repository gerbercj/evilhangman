//
//  HangmanAppDelegate.h
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface HangmanAppDelegate : NSObject <UIApplicationDelegate> {
}

@property (nonatomic) IBOutlet UIWindow *window;
@property (nonatomic) IBOutlet MainViewController *mainViewController;

@end
