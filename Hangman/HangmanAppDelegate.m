//
//  HangmanAppDelegate.m
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "HangmanAppDelegate.h"

#import "MainViewController.h"

@implementation HangmanAppDelegate

@synthesize window=_window;
@synthesize mainViewController=_mainViewController;

+ (void)initialize {
    if ([self class] == [HangmanAppDelegate class]) {
        
        // Register a default value for word length and max guesses
        NSDictionary *resourceDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithFloat:6.0], @"wordLength",
                                      [NSNumber numberWithFloat:14.0], @"maxGuesses",
                                      nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:resourceDict];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // make the view visible
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];
    return YES;
}


@end
