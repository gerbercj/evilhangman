//
//  MainViewController.h
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "FlipsideViewController.h"
#import "EvilHangmanModel.h"

@interface MainViewController : UIViewController <UIAlertViewDelegate, FlipsideViewControllerDelegate> {
}

@property (nonatomic, strong) EvilHangmanModel *evilHangmanModel;
@property (nonatomic, assign) BOOL isBusy;
@property (nonatomic) IBOutlet UITextField *keyboardProxy;
@property (nonatomic) IBOutlet UITextField *hangmanWord;
@property (nonatomic) IBOutlet UILabel *guessesRemaining;
@property (nonatomic) IBOutlet UILabel *lettersGuessed;
@property (nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (void)updateGuessesRemaining;
- (void)startNewGame;
- (IBAction)showInfo:(id)sender;
- (IBAction)newGame:(id)sender;
- (IBAction)respondToGuess:(id)sender;
- (void)guessLetterInBackground:(NSString *)letter;
- (void)endGame:(BOOL)didWin;

@end
