//
//  MainViewController.m
//  Hangman
//
//  Created by Chris Gerber on 4/6/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "MainViewController.h"
#import "EvilHangmanModel.h"

@implementation MainViewController

@synthesize evilHangmanModel=_evilHangmanModel;
@synthesize isBusy=_isBusy;
@synthesize keyboardProxy;
@synthesize hangmanWord;
@synthesize guessesRemaining;
@synthesize lettersGuessed;
@synthesize spinner;

- (void)updateGuessesRemaining
{
    // update UI with formatted string
    [self.guessesRemaining setText:[NSString stringWithFormat:@"Guesses remaining: %d",[self.evilHangmanModel guessesRemaining]]];
}

- (void)updateLettersGuessed
{
    // get string of guessed letters
    NSString *letters = [self.evilHangmanModel getGuessedLetters];
    
    // split to two lines and update UI
    self.lettersGuessed.text = [[[letters substringToIndex:13] stringByAppendingString:@"\n"] stringByAppendingString:[letters substringFromIndex:13]];
}

- (void)startNewGame
{
    // prevent user input
    self.isBusy = YES;
    
    // load user preferences
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // update UI with stored preferences
    NSUInteger wordLength = [defaults floatForKey:@"wordLength"];
    NSUInteger maxGuesses = [defaults floatForKey:@"maxGuesses"];

    // update UI based upon preferences
    self.hangmanWord.text = [self.evilHangmanModel setWordLength:wordLength andGuessesRemaining:maxGuesses];
    [self updateGuessesRemaining];
    
    // reset list of letters guessed
    [self updateLettersGuessed];
    
    // save longest word length
    [self.evilHangmanModel longestWord];
    
    // enable game play
    self.isBusy = NO;
    
    // show the keyboard
    [self.keyboardProxy becomeFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // create the Evil Hangman Model and have it load it's dictionary
    NSString *fileWithPath = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"plist"];

    self.evilHangmanModel = [[EvilHangmanModel alloc] initWithContentsOfFile:fileWithPath];
    
    // initialize the game state
    [self startNewGame];
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    // close the flip side with an animation
    [self dismissModalViewControllerAnimated:YES];
}

- (NSUInteger)getLongestWordLength
{
    // longest word length from EH model
    return [self.evilHangmanModel longestWord];
}

- (IBAction)showInfo:(id)sender
{   
    // show the flipside view
    FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
    controller.delegate = self;
    
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
    
}

- (IBAction)newGame:(id)sender {
    [self startNewGame];
}

- (IBAction)respondToGuess:(id)sender {
    // ignore guess if still busy
    if (self.isBusy == NO) {
        // prevent additional key processing while we work
        self.isBusy = YES;
        
        // if there is a character waiting
        if ([self.keyboardProxy.text length] > 0) {
            // get the first letter pressed
            NSString *guessedLetter = [[[self.keyboardProxy text] capitalizedString] substringToIndex:1];
            
            // clear the input field
            self.keyboardProxy.text = @"";
            
            // if the character waiting is a valid letter
            if ([guessedLetter characterAtIndex:0] >= 'A' && [guessedLetter characterAtIndex:0] <= 'Z') {
                
                // show the spinner
                [spinner startAnimating];
                
                // have EH model check letter in the background
                [self performSelectorInBackground:@selector(guessLetterInBackground:) withObject:guessedLetter];

            } else {
                // OK for more input
                self.isBusy = NO;
            }
        } else {
            // OK for more input
            self.isBusy = NO;
        }
    }
}

- (void)guessLetterInBackground:(NSString *)letter
{
    // background thread needs its own autorelease pool
    @autoreleasepool {

    // convert the guessed letter from NSString to char
        char guessedLetter = [letter characterAtIndex:0];

        // update the model with guess, and reflect result to UI
        self.hangmanWord.text = [self.evilHangmanModel respondToGuess:guessedLetter];
        
        // update our UI with the guesses to date
        [self updateLettersGuessed];
        
        // stop the spinner
        [spinner stopAnimating];
        
        // OK for more input now
        self.isBusy = NO;

        // update display with guesses remaining
        [self updateGuessesRemaining];

        // check for a win or loss
        if ([self.evilHangmanModel didWin]) {
            // if the user won, tell them
            [self endGame:YES];
        } else {
            // if the user is out of guesses
            if ([self.evilHangmanModel guessesRemaining] < 1 ) {
                // tell the user that they lost
                [self endGame:NO];
            }
        }
    
    }
}

- (void)endGame:(BOOL)didWin
{
    // prevent more input
    self.isBusy = YES;
    
    // create an alert that tells the player that they won/lost, revealing a possible solution if the user lost
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Game Over"
                              message:didWin?
                                @"You win this round...":
                                [NSString stringWithFormat:@"You are no match for my evil!\nThe word was %@.",[self.evilHangmanModel possibleWord]]
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // only support portrait orientation
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload
{
    // no need to preserve state of objects on unload
    [self setEvilHangmanModel:nil];
    [self setGuessesRemaining:nil];
    [self setLettersGuessed:nil];
    [self setHangmanWord:nil];
    [self setKeyboardProxy:nil];
    [self setSpinner:nil];
    [super viewDidUnload];
}


@end
