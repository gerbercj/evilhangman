//
//  NSString+EvilHangman.h
//  Hangman Controller
//
//  Created by Chris Gerber on 4/7/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (EvilHangman)
- (NSUInteger)placeHolderCount;
@end
