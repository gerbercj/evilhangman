//
//  NSString+EvilHangman.m
//  Hangman Controller
//
//  Created by Chris Gerber on 4/7/11.
//  Copyright 2011 theGerb.com. All rights reserved.
//

#import "NSString+EvilHangman.h"

@implementation NSString (EvilHangman)

- (NSUInteger)placeHolderCount
{
    // initialize the count of dashes
    NSUInteger count=0;
    
    // for each character in the string
    for (NSUInteger index=[self length]; index>0; index--) {
        // if the character is a dash
        if ( [self characterAtIndex:index-1] == '-' ) {
            // increase the count by one
            count++;
        }
    }
    
    // return the count of dashes
    return count;
}

@end
